import request from '@/config/request'
import upload from '@/config/upload'
import {localStorage} from '@/common/util'



// app更新
export const getGetVersion = (data) => request('api/v1/login_account', data);

/**
 * =====================================================================================================================================
 * 个人数据管理
 */

// 账号登录
export const loginAccount = (data) => request('api/v1/login_account', data, 'POST');
