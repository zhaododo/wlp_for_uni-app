Mock.mock({
    "status":1,
    "message":"登陆成功",
    "result":{
        "token":"123123123",
        "user_real_na":"王咻咻",
        "user_code":"002",
        "user_position_na":"收银员",
        "user_shop_na":"苍山店",
        "user_shop_id":1,
        "user_role_id":1,
        "user_role_auth":[
            {
                "menu_id": 1,
                "action_auth": [
                    {
                        "action_id": 1
                    },
                    {
                        "action_id": 2
                    }
                ]
            }
        ],
    },
})



Mock.mock({
    "token":"sdagweh12te2gui2ghefiu",
    "data":[
        {
            "fms_id":1,
            "fms_code":"12512512",
            "fms_date":"2018-03-11 13:02:29",
            "member_id":12,
            "member_na":"王先生",
            "product_count":2,
            "amount":20,
            "rec_send_flag":1,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":2,
            "fms_code":"112g4ge512",
            "fms_date":"2018-04-22 13:02:29",
            "member_id":12,
            "member_na":"陈先生",
            "product_count":6,
            "amount":20,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":3,
            "fms_code":"t24td2512",
            "fms_date":"2018-07-02 13:02:29",
            "member_id":12,
            "member_na":"叶小姐",
            "product_count":22,
            "amount":10,
            "rec_send_flag":1,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":4,
            "fms_code":"f1r512512",
            "fms_date":"2018-07-10 13:02:29",
            "member_id":12,
            "member_na":"黄先生",
            "product_count":12,
            "amount":20,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":5,
            "fms_code":"351512512",
            "fms_date":"2018-09-14 13:02:29",
            "member_id":12,
            "member_na":"陈先生",
            "product_count":17,
            "amount":20,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":6,
            "fms_code":"1346312512",
            "fms_date":"2018-10-10 13:02:29",
            "member_id":12,
            "member_na":"林先生",
            "product_count":5,
            "amount":20,
            "rec_send_flag":1,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":7,
            "fms_code":"1251123",
            "fms_date":"2018-11-11 13:02:29",
            "member_id":12,
            "member_na":"洪先生",
            "product_count":9,
            "amount":20,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":8,
            "fms_code":"12122312",
            "fms_date":"2018-12-12 13:02:29",
            "member_id":12,
            "member_na":"王小姐",
            "product_count":13,
            "amount":10,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":9,
            "fms_code":"1241252512",
            "fms_date":"2018-12-12 17:22:00",
            "member_id":12,
            "member_na":"陈小姐",
            "product_count":12,
            "amount":20,
            "rec_send_flag":1,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":10,
            "fms_code":"125412512",
            "fms_date":"2018-12-21 13:02:29",
            "member_id":12,
            "member_na":"陈先生",
            "product_count":18,
            "amount":20,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        },
        {
            "fms_id":11,
            "fms_code":"125235",
            "fms_date":"2018-12-28 13:02:29",
            "member_id":12,
            "member_na":"何先生",
            "product_count":1,
            "amount":20,
            "rec_send_flag":2,
            "fms_type_id":"12",
            "fms_type_na":"顺丰快递"
        }
    ],
    "perPage": 15
})

Mock.mock({
    "fms_id":125253,
    "fms_code":"t23uyt43h2342",
    "fms_date":"2018-05-12 21:51:54",
    "member_id":431324,
    "member_na":"王先生",
    "product_count":22,
    "pay_way_id":"2",
    "amount":20,
    "rec_send_flag":1,
    "fms_type_id":"212",
    "fms_type_na":"顺丰快递",
    "fms_man":"摄影苍山店",
    "fms_mobile":"18033988888",
    "fms_address":"福建省福州市仓山区万达广场1号楼1023",
    "fms_img":"/static/images/logo.png,/static/images/user.png,/static/images/pwd.png,/static/images/empty.png,/static/images/indexBack.png",
    "remark":"无"
})

Mock.mock({
    "token":"sdagweh12adgwe2ghefiu",
    "data":[
        {
            "bill_type_id":1,
            "bill_type_na":"静物",
        },
        {
            "bill_type_id":1,
            "bill_type_na":"外景",
        },
        {
            "bill_type_id":1,
            "bill_type_na":"夜景",
        },
        {
            "bill_type_id":1,
            "bill_type_na":"婚礼",
        },
        {
            "bill_type_id":1,
            "bill_type_na":"写真",
        },
        {
            "bill_type_id":1,
            "bill_type_na":"商品",
        }
    ]
})


Mock.mock({
    "token":"sdagweh12adgwe2ghefiu",
    "data":[
        {
            "bee_id":1,
            "code":"fd13r23r12",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":2,
            "code":"fdrger12",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":3,
            "code":"fd1wnetnb3r12",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":4,
            "code":"fd1tnrt12",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":5,
            "code":"gwerh34g34g",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":6,
            "code":"herhf3r3r",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":7,
            "code":"gfdbr3g4",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":8,
            "code":"egfbtrtbrt",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":9,
            "code":"gergbstbt",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":10,
            "code":"fdfg3412",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":11,
            "code":"g234g343r12",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        },
        {
            "bee_id":12,
            "code":"fh34h343r12",
            "account":"18033982000",
            "cname":"王乐乐",
            "shop_na":"摄影苍山万达店",
            "position":"摄影师",
            "mobile":"18033982292",
            "state_str":"正常"
        }
    ],
    "perPage": 15
})

Mock.mock({
    "token":"sdagweh12adgwe2ghefiu",
    "data":[
        {
            "id":1,
            "code": "002",
            "value":"摄影师"
        },
        {
            "id":2,
            "code": "003",
            "value":"化妆师"
        },
        {
            "id":3,
            "code": "005",
            "value":"化妆助手"
        },
        {
            "id":4,
            "code": "008",
            "value":"摄影助手"
        },
        {
            "id":5,
            "code": "012",
            "value":"场景布置"
        },
        {
            "id":6,
            "code": "015",
            "value":"服装师"
        },
        {
            "id":7,
            "code": "019",
            "value":"灯光师"
        },
        {
            "id":8,
            "code": "021",
            "value":"灯光助手"
        }
    ]
})



Mock.mock({
    "token":"sdagweh12adgwe2ghefiu",
    "data":[
        {
            "bill_id":123,
            "bill_code":"125f32f12",
            "bill_na":"全套室内外婚纱写真",
            "bill_date":"2018-12-28",
            "member_id":1,
            "member_na":"王先生",
            "product_count":260,
            "bill_type_id":"1",
            "bill_type_na":"写真",
            "bill_work":"",
            "bill_state":"3",
            "bill_state_str":"已完成"
        },
        {
            "bill_id":123,
            "bill_code":"125f32f12",
            "bill_na":"全套室内外婚纱写真",
            "bill_date":"2018-12-16",
            "member_id":1,
            "member_na":"王先生",
            "product_count":260,
            "bill_type_id":"1",
            "bill_type_na":"写真",
            "bill_work":"待客户收件",
            "bill_state":"2",
            "bill_state_str":"已完成"
        },
        {
            "bill_id":123,
            "bill_code":"125f32f12",
            "bill_na":"全套室内外婚纱写真",
            "bill_date":"2018-11-29",
            "member_id":1,
            "member_na":"王先生",
            "product_count":260,
            "bill_type_id":"1",
            "bill_type_na":"写真",
            "bill_work":"完成拍摄，待修图",
            "bill_state":"2",
            "bill_state_str":"进行中"
        },
    ],
    "perPage": 15
})

Mock.mock({
        "deal_custom_num":11,
        "deal_product_num":14,
        "deal_photo_num":425,
        "profit_amt":"5230",
        "bill_profit_list":[{
            "bill_profit_id":"124",
            "bill_id":1,
            "bill_code":"124df323f",
            "bill_na":"外派摄影",
            "bill_deal_at":"2018-10-07",
            "bill_product_count":12,
            "bill_photo_num":124,
            "profit_amount":200,
            "work_id":1,
            "work_na":"写真",
            "profit_unit_id":1,
            "profit_unit_na":"摄影仓山店"
        }]
    })

Mock.mock({
    "data":[{
        "notice_id":"1421",
        "title":"春节放假通知",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知2",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知3",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知4",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知5",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知2",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知3",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知4",
        "publish_at":"2018-12-29"
    },{
        "notice_id":"1421",
        "title":"春节放假通知5",
        "publish_at":"2018-12-29"
    }],
    "perPage":15
})

Mock.mock({
    "notice_id": "1421",
    "title": "春节放假通知",
    "publish_at": "2018-12-29",
    "content": "春节放假通知,大家快跑，啦啦啦啦！",
    "publisher": "老板",
    "view_num": 1
})

Mock.mock({
    "deal_custom_num":11,
    "deal_product_num":14,
    "deal_photo_num":425,
    "profit_amt":"5230",
    "bill_profit_list":[{
        "bill_profit_id":"124",
        "bill_id":1,
        "bill_code":"124df323f",
        "bill_na":"外派摄影",
        "bill_deal_at":"2018-10-07",
        "bill_product_count":12,
        "bill_photo_num":124,
        "profit_amount":200,
        "work_id":1,
        "work_na":"写真",
        "profit_unit_id":1,
        "profit_unit_na":"摄影仓山店"
    }]
})


Mock.mock({
    "bill_id":1,
    "bill_code":"f223f123f12d12",
    "bill_na":"摄影苍山店",
    "bill_date":"2018-06-22",
    "member_id":123,
    "member_na":"王先生",
    "product_count":12,
    "bill_type_id":1,
    "bill_type_na":"外景",
    "bill_state":2,
    "bill_state_str":"进行中",
    "bill_detial_list":[{
        "detail_at":"2018-06-22",
        "detail_bee_id":124,
        "detail_bee_na":"张三",
        "detail_work_id":1,
        "detail_work_na":"修图师",
        "detail_work_content":"导图较色完成",
        "detail_remark":"客户需要第二套，延迟进度。"
    },{
        "detail_at":"2018-06-22",
        "detail_bee_id":124,
        "detail_bee_na":"王乐乐",
        "detail_work_id":1,
        "detail_work_na":"摄影师",
        "detail_work_content":"拍摄完成",
        "detail_remark":""
    },{
        "detail_at":"2018-06-22",
        "detail_bee_id":124,
        "detail_bee_na":"秋秋",
        "detail_work_id":1,
        "detail_work_na":"化妆师",
        "detail_work_content":"化妆完成",
        "detail_remark":""
    }],
})

摄影系统-获取项目记录详情信息
摄影系统-项目单操作
通用字典key work_content 无法测试 接口重合，对接线上时要修改project-info.vue页面内picker控件显示数据（目前字段为value,id）



线上api测试bug
绩效页面 GetBeeProfitTotal 参数




Mock.mock({
        "total_debt_amt":124212,
        "last_debt_amt":12412,
        "debt_amt":12412,
        "payment_amt":15212,
        "custom_bill_list":[{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "debt_amt":12421,
        }],
        "receipt_bill_list":[{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        },{
            "bill_deal_at":"1991-07-17 08:13:25",
            "bill_code":"jXpV[xt",
            "bill_amt":12421,
        }]
})

Mock.mock({
    "data":[
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        },
        {
            "smt_notice_id":"1",
            "title":"耐克鞋店2018年6月份账单通知",
            "create_at":"2018-6-22 13:22:32",
            "diary_at":"2018-6",
            "shop_id":"1",
            "confirm_flag":"1"
        }
    ],
    "perPage":15
})

Mock.mock({
    "data":[
        {
            "order_id":124,
            "order_code":"zC7bd",
            "order_at":"1977-12-28 10:22:46",
            "order_pcode":"K@baZTJ",
            "order_type_id":3,
            "order_type_na":"项目单收款",
            "order_member_id":1,
            "order_member_na":"耐克鞋店",
            "pay_way_na":"现金",
            "order_amt":120
        },
        {
            "order_id":124,
            "order_code":"zC7bd",
            "order_at":"1977-12-28 10:22:46",
            "order_pcode":"K@baZTJ",
            "order_type_id":2,
            "order_type_na":"挂账收款-按金额",
            "order_member_id":1,
            "order_member_na":"耐克鞋店",
            "pay_way_na":"现金",
            "order_amt":140
        },
        {
            "order_id":124,
            "order_code":"zC7bd",
            "order_at":"1977-12-28 10:22:46",
            "order_pcode":"K@baZTJ",
            "order_type_id":1,
            "order_type_na":"到付收款",
            "order_member_id":1,
            "order_member_na":"耐克鞋店",
            "pay_way_na":"现金",
            "order_amt":140
        },
        {
            "order_id":124,
            "order_code":"zC7bd",
            "order_at":"1977-12-28 10:22:46",
            "order_pcode":"K@baZTJ",
            "order_type_id":4,
            "order_type_na":"销售收款",
            "order_member_id":1,
            "order_member_na":"耐克鞋店",
            "pay_way_na":"现金",
            "order_amt":140
        },
    ],
    "perPage":15
})

选择项目操作
暂停项目/项目完结/项目完成
boss_content

项目单收款就是销售收款-3，挂账收款是2，
然后销售单和按单的话，id都是传到sale_order_list里面。

新增会员的code是用户手动填的，member_id是修改用的，
ty我写补充上了。我把会员的那些字段名称给标注上了。