/*
 * ============================================================
 * 检查更新
 */
import {getGetVersion} from '@/service/getData'

export default function() {
    const res = uni.getSystemInfoSync();
    getGetVersion().then((resVersion)=>{
        if (resVersion.status == 1) {
            // #ifndef APP-PLUS
            let open = res.platform === 'android' ? resVersion.result.Android : resVersion.result.iOS;
            uni.showModal({ 
                title: open.title ? open.title : 'app下载',
                content: open.note ? open.note : '使用app更流畅',
                confirmText: '下载',
                success: (resModal) => {
                    if (resModal.confirm) {
                        uni.redirectTo({
                            url:open.url
                        })
                    }
                }
            });
            // #endif
            // #ifdef APP-PLUS
            if(res.platform === 'ios'){
                let open = resVersion.result.iOS;
                if(open.version!=res.version){
                    plus.runtime.openURL(open.url);
                }
            }else if(res.platform === 'android'){
                let open = resVersion.result.Android;
                if(open.version!=res.version){
                    // 提醒用户更新
                    uni.showModal({ 
                        title: open.title ? open.title : '更新提示',
                        content: open.note ? open.note : '是否选择更新',
                        success: (resModal) => {
                            if (resModal.confirm) {
                                uni.showLoading({
                                    title: '下载中',
                                    mask:true
                                });
                                uni.downloadFile({
                                    url: open.url, 
                                    success: (resFile) => {
                                        if (resFile.statusCode === 200) {
                                            // 调用第三方程序打开指定的临时文件
                                            plus.runtime.openFile( resFile.tempFilePath,{}, function(){
                                                uni.showToast({
                                                    title: '安装失败',
                                                    duration: 2000
                                                });
                                            });
                                        }
                                    },
                                    complete: (resFile) => {
                                        uni.hideLoading();
                                    }
                                });
                            }
                        }
                    })
                }
            }
            // #endif
        }
    });
}