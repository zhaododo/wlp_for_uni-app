# wlp_uni-app

## 项目介绍
基于uni-app开发组件、模板、功能探索


## 软件架构
软件架构说明
Hbuilder uni-app
vue
vuex


## 项目运行
HbuilderX真机运行
微信小程序


## 目标功能
- [x] 用户首页
- [ ] 收银员首页
- [ ] 管理首页
- [ ] 系统管理首页

- [ ] 页面权限

- [ ] 首次进入展示页、引导页
- [x] 登录
- [ ] 微信登录
- [ ] 发送短信
- [ ] 修改密码
- [ ] 个人中心
- [ ] 上传头像
- [ ] 订单列表
- [ ] 订单详情
- [ ] 下单功能
- [ ] 购物车功能
- [ ] 定位功能
- [ ] 选择城市
- [ ] 搜索地址
- [ ] 下载App

## 项目布局

### 主体结构
```
.
├── common                                  // 公共文件
│   ├── update.js                               // app版本升级
│   └── util.js                                 // 公共函数
├── components                              // 组件
├── config                                  // 基本配置
│   ├── env.js                                  // 环境切换配置
│   ├── request.js                              // 请求方法
│   ├── upload.js                               // 文件上传方法
│   └── errorLog.js                             // 网络请求错误处理
├── pages                                   // 页面
│   ├── api                                     // 功能参考页面
│   ├── component                               // 组件参考页面
│   ├── tabBar                                  // tabBar
│   └── template                                // uni-app template其他风格
├── service                                 // 数据交互统一调配
│   ├── getData.js                              // 获取数据的统一调配文件，对接口进行统一管理
│   └── tempdata                                // 开发阶段的临时数据
├── static                                  // 静态文件
│   ├── font                                    // 字体图标
│   ├── icon                                    // 图标
│   ├── img                                     // 图片
│   └── temp                                    // 临时图片
├── store                                   // vuex的状态管理
│   ├── action.js                               // 配置actions
│   ├── getters.js                              // 配置getters
│   ├── index.js                                // 引用vuex，创建store
│   ├── mutation-types.js                       // 定义常量muations名
│   └── mutations.js                            // 配置mutations
├── style                                   // 样式文件
├── unpackage                               // 临时文件
│   └── screenshots                         // 项目参考截图
├── App.vue                                 // 页面入口文件
├── main.js                                 // 程序入口文件，加载各种公共组件
├── manifest.json                           // 应用配置
├── pages.json                              // 页面配置
├── README.md                               // 自述文件
├── uni.scss                                // 常用样式变量
.
0 directories, 0 files
```

### 样式style
```
.
├── component               // 组件样式
│   ├── icon.scss           // icon
│   ├── card-banner         // 首页banner
│   ├── card-good           // 首页活动商品
│   ├── grid-scroll         // 首页横向grid
│   └── numbox-shopping     // 商品加入购物车按钮
├── view                    // 页面样式
│   ├── grid-scroll         // 首页横向grid
│   └── numbox-shopping     // 商品加入购物车按钮
├── base.scss               // 样式入口
└── uni.css                 // uni组件样式
```

### components组件参考页面介绍
- [x] ├── swiper-banner             [轮播图banner(可调整高宽百分比)](unpackage/README/components/swiper-banner.md)
- [x] ├── grid-scroll               [首页宫格入口(横向展示)](unpackage/README/components/grid-scroll.md)
- [x] ├── numberBox-shopping        [数字输入框(购物车)](unpackage/README/components/numbox-shopping.md)
- [x] ├── list-good                 [商品列表](unpackage/README/components/list-good.md)
- [x] ├── list-good-swiper          [商品列表(购物车)](unpackage/README/components/list-good-swiper.md)
- [x] ├── load-more                 // 底部加载
- [x] ├── uni-countdown             // uni倒计时
- [x] ├── uni-icon                  // uni图标
- [x] ├── uni-nav-bar               // uni头部
- [x] ├── uni-status-bar            // uni顶部状态栏
- [ ] ├── search          // 弹出搜索页组件
- [ ] ├── alertTip        // 弹出框组件
- [ ] ├── buyCart         // 购物车组件
- [ ] ├── computeTime     // 倒计时组件
- [ ] ├── loading         // 页面初始化加载数据的动画组件
- [ ] ├── mixin.js            // 组件混合(包括：指令-下拉加载更多，处理图片地址)
- [ ] ├── ratingStar      // 评论的五颗星组件
- [ ] └── shoplist        // msite和shop页面的餐馆列表公共组件


### pages-template页面介绍
- [x] template
- [x] ├── activity                        // 活动
- [ ] │   ├── coupons                         // 优惠券
- [ ] │   ├── invite                          // 邀请
- [ ] │   ├── share                           // 分享
- [ ] │   └── timeLimit                       // 限时抢购
- [x] ├── address                         // 地址
- [ ] │   ├── add                             // 添加地址
- [ ] │   ├── choose                          // 选择地址
- [ ] │   ├── city                            // 当前城市页
- [ ] │   ├── edit                            // 编辑地址
- [ ] │   ├── list                            // 地址列表
- [ ] │   └── searchMap                       // 地图搜索地址
- [x] ├── article                         // 文章
- [ ] │   ├── comment                         // 文章评论页
- [ ] │   ├── list                            // 文章列表页
- [ ] │   └── detail                          // 文章详情页
- [x] ├── bankCard                         // 银行卡
- [ ] │   ├── bind                            // 银行卡绑定页
- [ ] │   └── list                            // 银行卡列表页
- [x] ├── bid                             // 竞标
- [ ] │   └── selectShop                      // 选择竞标商铺
- [x] ├── error                           // 错误页
- [ ] │   └── 404                             // 404
- [x] ├── good                            // 商品
- [ ] │   ├── classify                        // 商品分类页
- [ ] │   ├── collection                      // 商品收藏页
- [ ] │   ├── detail                          // 商品详情页
- [ ] │   ├── evaluate                        // 商品评论页
- [ ] │   ├── history                         // 商品历史记录页
- [ ] │   ├── list                            // 商品筛选排序页
- [ ] │   └── search                          // 商品搜索页
- [x] ├── member                          / 会员
- [ ] │   ├── integral                        // 积分
- [ ] │   └── vip                             // 会员
- [x] ├── message                         / 消息
- [ ] │   ├── detail                          // 消息详情
- [ ] │   ├── im-chat                         // 聊天窗口
- [ ] │   ├── list                            // 消息列表
- [ ] │   ├── notice                          // 公告
- [ ] │   └── service                         // 客服
- [x] ├── order                           // 订单
- [ ] │   ├── detail                          // 订单详情页
- [ ] │   ├── evaluate                        // 订单评论页
- [ ] │   └── tabList                         // 订单列表tab页
- [x] ├── setting                         // app管理
- [ ] │   ├── about                           // 关于
- [ ] │   ├── download                        // 下载app
- [ ] │   ├── feedback                        // 问题反馈
- [ ] │   ├── invite                          // 邀请
- [ ] │   ├── log                             // 日志管理
- [ ] │   ├── setting                         // 设置
- [ ] │   └── share                           // 分享
- [x] ├── shop                            // 商铺
- [ ] │   ├── classify                        // 商铺分类页
- [ ] │   ├── collection                      // 商铺收藏页
- [ ] │   ├── detail                          // 商铺详情页
- [ ] │   ├── evaluate                        // 商铺评论页
- [ ] │   ├── history                         // 商铺历史记录页
- [ ] │   ├── list                            // 商铺筛选排序页
- [ ] │   └── search                          // 商铺搜索页
- [x] ├── shopping                        // 购物
- [ ] │   ├── invoice                         // 发票
- [ ] │   ├── logistics                       // 物流消息
- [ ] │   ├── order                           // 下订单
- [ ] │   ├── pay                             // 支付
- [ ] │   └── shoppingCart                    // 购物车
- [x] ├── tabBar                          // 标签页
- [ ] │   ├── home                            // 首页
- [ ] │   ├── classify                        // 分类
- [ ] │   ├── find                            // 发现页
- [ ] │   ├── 指向shopping/shoppingCart       // 购物车
- [ ] │   └── 指向user/profile                // 我的
- [x] ├── user                            // 用户
- [ ] │   ├── businessCard                    // 名片
- [ ] │   ├── forget                          // 忘记密码，修改密码页
- [ ] │   ├── info                            // 帐户信息
- [ ] │   ├── login                           // 登录页
- [ ] │   ├── profile                         // 个人中心
- [ ] │   └── reg                             // 注册页
- [x] └── wallet                          // 钱包
- [ ]     ├── balance                         // 余额
- [ ]     ├── detailList                      // 资金明细
- [ ]     ├── recharge                        // 充值
- [ ]     └── withdrawal                      // 提现




## 备忘录

- [x] ├── 登陆
- [ ] │   ├── 忘记密码
- [ ] ├── 收银员
- [ ] │   ├── 收银
- [ ] │   │   ├── 销售单
- [ ] │   │   │   ├── 商品筛选
- [ ] │   │   │   ├── 客户选择
- [ ] │   │   │   └── 结算
- [ ] │   │   │       ├── 支付方式
- [ ] │   │   │       ├── 是否配送
- [ ] │   │   │       ├── 是否仓库发货
- [ ] │   │   │       ├── 是否开票
- [ ] │   │   │       ├── 优惠券
- [ ] │   │   │       └── 折扣
- [ ] │   │   ├── 已有挂单
- [ ] │   │   └── 销售退货单
- [ ] │   ├── 客户管理
- [ ] │   │   ├── 新增个人客户
- [ ] │   │   ├── 新增单位客户
- [ ] │   │   └── 新增会员卡
- [ ] │   ├── 营业日结
- [ ] │   │   └── 日结记录
- [ ] │   ├── 到货确认
- [ ] │   │   ├── 到货入库
- [ ] │   │   ├── 进货单详情
- [ ] │   │   └── 入库记录
- [ ] │   │       └── 入库单详情
- [ ] │   ├── 仓库盘点
- [ ] │   │   ├── 盘点
- [ ] │   │   └── 盘点记录
- [ ] │   │       └── 盘点详情
- [ ] │   ├── 销货单查询
- [ ] │   │   └── 销货单详情
- [ ] │   └── 业绩查询
- [ ] ├── 店长
- [ ] │   ├── 商品管理
- [ ] │   │   └── 新增商品
- [ ] │   │       ├── 分类设置
- [ ] │   │       │   └── 新增分类
- [ ] │   │       ├── 单位设置
- [ ] │   │       │   └── 新增单位
- [ ] │   │       └── 价格设置
- [ ] │   ├── 总部进货
- [ ] │   │   ├── 商品筛选
- [ ] │   │   ├── 进货结算
- [ ] │   │   └── 进货单记录
- [ ] │   │       └── 进货单详情
- [ ] │   ├── 库存查询
- [ ] │   ├── 员工管理
- [ ] │   │   └── 新增职员
- [ ] │   ├── 业绩设置
- [ ] │   ├── 发布公告
- [ ] │   │   └── 新增公告
- [ ] │   ├── 会员卡设置
- [ ] │   │   ├── 已领会员卡
- [ ] │   │   └── 新增会员卡卡种
- [ ] │   │       ├── 编号选择
- [ ] │   │       └── 有效期
- [ ] │   └── 供应商管理
- [ ] │       └── 新增供应商
- [ ] ├── 财务
- [ ] │   ├── 收款
- [ ] │   │   ├── 挂账收款-按金额
- [ ] │   │   ├── 挂账收款-按金额
- [ ] │   │   └── 收款记录
- [ ] │   ├── 付款
- [ ] │   │   ├── 进货挂账付款-按金额
- [ ] │   │   ├── 进货挂账付款-按单
- [ ] │   │   └── 付款记录
- [ ] │   ├── 其他收入/支出
- [ ] │   │   └── 其他收入/支出记录
- [ ] │   ├── 日结记录
- [ ] │   └── 应收/应付
- [ ] │       ├── 往来账-应收
- [ ] │       └── 往来账-应付
- [ ] ├── 仓管
- [ ] │   ├── 销售出库
- [ ] │   │   ├── 确认出库
- [ ] │   │   ├── 出库记录
- [ ] │   │   │   └── 出库单详情
- [ ] │   │   └── 销售单出库详情
- [ ] │   ├── 其他出/入库
- [ ] │   │   └── 其他出/入库记录
- [ ] │   │       └── 其他出/入库详情
- [ ] │   └── 库存汇总
- [ ] ├── BOSS
- [ ] │   └── 营运报表
- [ ] │       ├── 产品销售日报
- [ ] │       ├── 产品销售月报
- [ ] │       ├── 会员购买月报
- [ ] │       ├── 店铺收益月报
- [ ] │       ├── 进货报表
- [ ] │       └── 库存报表
- [ ] └── 系统管理员
- [ ]     ├── 账号管理
- [ ]     │   └── 新增账号
- [ ]     ├── 角色/权限管理
- [ ]     │   └── 新增角色
- [ ]     ├── 菜单/功能管理
- [ ]     │   └── 新增菜单
- [ ]     ├── 门店/仓库管理
- [ ]     │   └── 新增门店/仓库
- [ ]     ├── 其他收入/支出管理
- [ ]     │   └── 新增其他收入/支出类型
- [ ]     ├── 银行账号管理
- [ ]     │   └── 新增银行账号
- [ ]     ├── 物流公司管理
- [ ]     │   └── 新增物流公司
- [ ]     ├── 系统参数
- [ ]     └── 日志管理


```
====================================================================================
│   ├── confirmOrder
│   │   ├── children
│   │   │   ├── chooseAddress.vue           // 选择地址页
│   │   │   ├── invoice.vue                 // 选择发票页
│   │   │   ├── payment.vue                 // 付款页
│   │   │   ├── remark.vue                  // 订单备注页
│   │   │   └── userValidation.vue          // 用户验证页
│   │   └── confirmOrder.vue                // 确认订单页
│   ├── shop
│   │   ├── children
│   │   │   ├── children
│   │   │   │   └── shopSafe.vue            // 商铺认证信息页
│   │   │   ├── foodDetail.vue              // 商铺信息页
│   │   │   └── shopDetail.vue              // 单个商铺信息页
│   │   └── shop.vue                        // 商铺筛选页
│   ├── benefit
│   │   ├── benefit.vue                     // 红包页
│   │   └── children
│   │       ├── commend.vue                 // 推荐有奖
│   │       ├── coupon.vue                  // 代金券说明
│   │       ├── exchange.vue                // 兑换红包
│   │       ├── hbDescription.vue           // 红包说明
│   │       └── hbHistory.vue               // 历史红包
│   └── vipcard
│       ├── children
│       │   ├── invoiceRecord.vue           // 购买记录
│       │   ├── useCart.vue                 // 使用卡号购买
│       │   └── vipDescription.vue          // 会员说明
│       └── vipcard.vue                     // 会员卡办理页

    运营管理
        店铺管理
        客户管理
        会员管理
        项目管理
            列表
            详情
                项目功能操作
            新增/修改
        基础设置
        人员管理
        权限管理
        菜单管理
        抽成管理
        绩效管理
        账单管理
        收款管理
        收发快件fms
            列表
            详情
            新增/修改
        财务管理
        仓库管理
        物流管理
    活动
        优惠券
        邀请
        分享
        限时
        拼团
        抽奖
        红包
    三级分销
        分成收益
```


