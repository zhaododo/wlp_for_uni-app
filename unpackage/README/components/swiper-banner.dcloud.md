## 轮播图Banner

**轮播图高宽比默认1：1 /由参数height（默认：100）控制**

#### 截图

<img src="https://gitee.com/wlp886/wlp_for_uni-app/raw/master/unpackage/README/components/swiper-banner.jpg" width="375" height="667"/>

#### [示例](https://gitee.com/wlp886/wlp_for_uni-app/blob/master/pages/component/swiper-banner/swiper-banner.vue)

#### 事件
click(索引)

#### 属性
```
data: { // 图片链接
    type: Array,
    default: function(e) {
        return [];
    }
},
height: { // 高宽百分比
    type: Number,
    default: function(e) {
        return 100;
    }
}
```
