## 轮播图Banner

**轮播图高宽比默认1：1 /由参数height（默认：100）控制**

#### 截图

<img src="./grid-scroll.jpg" width="375" height="177"/>

#### [示例](../../../pages/component/grid-scroll/grid-scroll.vue)

#### 事件
click(索引)

#### 属性
```
data: {
    imgUrl: '/static/temp/grid (1).jpeg',
    title: '甜品饮品'
}
```
