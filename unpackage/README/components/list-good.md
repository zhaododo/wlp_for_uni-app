## 商品列表

## 截图

<img src="./list-good1.jpg" width="375" height="667"/>

#### **slot插入任意数据或购物车按钮插入文字**  
<img src="./list-good2.jpg" width="375" height="667"/>

#### **切换显示列数单列**  
<img src="./list-good3.jpg" width="375" height="667"/>

#### **切换显示列数多列（默认双列：由width参数控制）**  
<img src="./list-good4.jpg" width="375" height="667"/>

### [示例](../../../pages/component/list-good/list-good.vue)

### 依赖
**依赖[flex.scss](../../../style/mixin/flex.scss)**  
**依赖[hr.scss](../../../style/mixin/hr.scss)**  
**依赖[text-overflow.scss](../../../style/mixin/text-overflow.scss)**  
**依赖[price-before.scss](../../../style/mixin/price-before.scss)**  
**依赖uni-app组件uni-icon、uni-badge**  

### 注意
**注意：索引可输入数组来支持多层循环**  
**注意：data不包含size时不显示购物车按钮**  
**注意：data.sku.length>1时 购物车按钮数量显示右上角badge**  
**注意：data.size小于0时可自定义购物车按钮内容/slot="numbox"**  
**注意：可自定义颜色：详情看组件css**  

### 事件
click(索引, 点击位置)
change(数量值, 索引)

### 属性
```
type: { // 显示类型：column（单列）、row（多列）
    type: String,
    default: 'column'
},
data: { // 商品属性
    type: Object
    default: {
        title: '标题',
        titleSub: '副标题', // 可选
        label: '优惠标签', // 可选
        imgUrl: '图片地址',
        sku: [{name:'规格'}], // 可选
        price: '价格',
        originalPrice: '原价', // 可选
    }
},
index: { // 索引
    type: [Number, Array],
    default: 0
},
width: { // type为row时，多列显示
    type: String,
    default: '50%'
}
// 购物车按钮
max: { // 最大值
    type: Number,
    default: Infinity
},
step: { // 步长
    type: Number,
    default: 1
},
disabled: { // 是否禁用键盘输入
    type: Boolean,
    default: false
}
```
