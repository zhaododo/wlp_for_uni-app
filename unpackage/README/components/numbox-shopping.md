## 购物车数字输入框

#### 截图
<img src="./numbox-shopping.jpg" width="375" height="667"/>

#### [示例](../../../pages/component/numbox-shopping/numbox-shopping.vue)

**注意：依赖[flex.scss](../../../style/mixin/flex.scss)**  
**注意：依赖uni-app组件uni-icon**  
**注意：索引可输入数组来支持多层循环**  
**注意：可自定义颜色**`$uni-color-primary/$uni-color-warning/$uni-color-error`  
**注意：固定最小值限制0**  

#### 事件
默认：change(value, 索引)  
value值为-1或-2：click(索引)

#### 属性
```
value : { // 输入值
  type : Number,
  value : 0 
},
index: { // 组件索引
    type: [Number, Array],
    default: 0
},
disabled : { // 是否禁用键盘输入
  type : Boolean,
  value : false
},
max:{ // 最大值
  type: Number,
  value: 20
},
step: { // 步长
    type: Number,
    default: 1
},
```
