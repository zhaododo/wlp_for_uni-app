/*
 * =========================================================
 * 异常处理
 * 
*/

export default (objError) => {
    try {
        return new Promise((resolve, reject) => {
            switch(objError.type){
                case 'status':
                    if(objError.data.status==0){
                        // console.log("toKen失效");
                        uni.showToast({
                            title: '长时间未登陆，请重新登陆',
                            icon:"none",
                        });
//                         setTimeout(function(){
//                             uni.reLaunch({
//                                 url:'/pages/user/login/login'
//                             });
//                         },1200);
                    }else if(objError.data.status==2){
                        // console.log("api提交失败");
                        uni.showToast({
                            title: objError.data.message,
                            icon:"none",
                        });
                    }else if(objError.data.status>2){
                        // console.log("api验证失败");
                        uni.showToast({
                            title: objError.data.message,
                            icon:"none",
                        });
                    }
                    resolve(objError)
                    break;
                case 'statusCode':
                    console.log("api提交错误:"+objError.data.statusCode);
                    console.log(objError.data);
                    break;
                case 'error':
                    console.log("error");
                    break;
            }
        });
    } catch (error) {
        throw new Error(error)
    }
}