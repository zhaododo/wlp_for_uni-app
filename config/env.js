/**
 * 配置编译环境和线上环境之间的切换
 * 
 * baseUrl: 域名地址
 * imgBaseUrl: 图片所在域名地址
 * 
 */

const baseUrl = 'https://xxx.xxx.com/';
const imgBaseUrl = 'http://xxx.xxx.com/storage/image/';

export {
    baseUrl,
    imgBaseUrl,
}