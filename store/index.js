import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'
import getters from './getters'

Vue.use(Vuex)

const state = {
	firstApp: false,		// 首次使用App
	location: {
		latitude: '',		// 当前位置纬度
		longitude: '',		// 当前位置经度
		guessCity: '',   	// 当前城市
		guessCityid: '', 	// 当前城市id
	},
    forcedLogin: false,     // 是否强制登陆
    hasLogin: false,        // 是否登陆
    userInfo: null,         // 用户信息
    cartList: {},           // 加入购物车的商品列表
	version: "1.1.9",		// 当前版本
}

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
})