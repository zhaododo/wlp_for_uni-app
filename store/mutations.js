import {
    REG_PHONE,                // 手机号判断
    LOGOUT,                    // 注销
    RECORD_USERACCOUNT,        // 记录登陆信息
    RECORD_USERINFO,        // 记录用户信息
    GET_PICKERFMSLIST,        // 记录picker快递公司信息
    GET_PICKERBILLTYPELIST,    // 记录picker快递公司信息
    GET_PICKERJOBLIST,        // 记录picker职位信息
    GET_PICKERBILLWORKLIST,    // 记录picker项目单职位抽成信息
    GET_PICKERWORKCONTENTLIST,    // 记录picker项目单工作状态信息
    GET_PICKERBOSSCONTENTLIST,    // 记录picker项目单项目状态信息
    GET_PICKERPAYMENTLIST,    // 记录picker支付方式信息
    GET_PICKERSHOPLIST,        // 记录picker门店列表信息
    GET_PICKERSHOPROLELIST,    // 记录picker门店角色列表信息
    TEMP_RES,                // 新增刷新判断
    TEMP_MANAGE,            // 缓存选中客户
    TEMP_RECFMS,            // 缓存选中收件单
    TEMP_PEOPLE,            // 缓存选中相关人员
    TEMP_PROJECT,            // 缓存选中项目单
} from './mutation-types.js'

import {localStorage} from '../common/util'

import {localapi, proapi} from '../config/env'

export default {
    // 新增刷新判断
    [TEMP_RES](state, data) {
        state.tempRes=data;
    },
    // 手机号判断
    [REG_PHONE](state, phone) {
        let reg=/^1[3456789]\d{9}$/;
        if(phone!=''&&reg.test(phone))
        {
            return true;
        }
        return false;
    },
    // 记录登陆信息
    [RECORD_USERACCOUNT](state, account) {
        localStorage.setStore('user_account', account);
    },
    // 注销清除登陆信息
    [LOGOUT](state) {
        uni.showModal({
            "title": "注销",
            "content": "注销后请重新登陆！",
            "success": function(res){
                if (res.confirm) {
                    localStorage.clearStore();
                    state.login = false;
                    state.latitude = ''; 
                    state.longitude = '';
                    state.jurisdiction = {};
                    state.pickerFmsList= [];        
                    state.pickerBillTypeList = [];    
                    state.pickerJobList = [];        
                    state.pickerShopRoleList = [];    
                    state.pickerBillWorkList = [];    
                    state.pickerWorkContentList = [];    
                    state.pickerBossContentList = [];    
                    state.pickerPaymentList = [];    
                    state.pickerShopList = [];        
                    state.tempRes = false;            
                    state.tempMember = null;        
                    state.tempRecFms = null;        
                    state.tempPeople = null;        
                    state.tempProject = null;    
                    uni.reLaunch({
                        url:'/pages/user/login/login'
                    });    
                }
            }
        })
    },
    // 记录用户信息
    [RECORD_USERINFO](state, info) {
        for(let i = 0; i < info.user_role_auth.length;i++){
            switch(info.user_role_auth[i].menu_id){
                case 1:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 1:
                                state.jurisdiction.index_projectDataMonth = true;
                                break;
                            case 2:
                                state.jurisdiction.index_receivableDataMonth = true;
                                break;
                            case 3:
                                state.jurisdiction.index_reminderData = true;
                                break;
                        }
                    }
                    break;
                case 2:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 4:
                                state.jurisdiction.fms_see = true;
                                break;
                            case 5:
                                state.jurisdiction.fms_confirmReceipt = true;
                                break;
                        }
                    }
                    break;
                case 3:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 6:
                                state.jurisdiction.project_confirmNew = true;
                                break;
                        }
                    }
                    break;
                case 4:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 7:
                                state.jurisdiction.project_see = true;
                                break;
                            case 8:
                                state.jurisdiction.project_confirmStaff = true;
                                break;
                            case 9:
                                state.jurisdiction.project_confirmBoss = true;
                                break;
                        }
                    }
                    break;
                case 5:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 10:
                                state.jurisdiction.project_seeHistory = true;
                                break;
                        }
                    }
                    break;
                case 6:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 11:
                                state.jurisdiction.performance_seeThisMonth = true;
                                break;
                        }
                    }
                    break;
                case 7:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 12:
                                state.jurisdiction.message_see = true;
                                break;
                            case 13:
                                state.jurisdiction.message_del = true;
                                break;
                            case 14:
                                state.jurisdiction.message_confirm = true;
                                break;
                        }
                    }
                    break;
                case 8:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 15:
                                state.jurisdiction.collection_confirm = true;
                                break;
                            case 16:
                                state.jurisdiction.collection_see = true;
                                break;
                        }
                    }
                    break;
                case 9:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 17:
                                state.jurisdiction.client_see = true;
                                break;
                            case 18:
                                state.jurisdiction.client_add = true;
                                break;
                            case 19:
                                state.jurisdiction.client_modify = true;
                                break;
                            case 20:
                                state.jurisdiction.client_del = true;
                                break;
                        }
                    }
                    break;
                case 10:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 21:
                                state.jurisdiction.people_see = true;
                                break;
                            case 22:
                                state.jurisdiction.people_add = true;
                                break;
                            case 23:
                                state.jurisdiction.people_modify = true;
                                break;
                            case 24:
                                state.jurisdiction.people_del = true;
                                break;
                            case 25:
                                state.jurisdiction.people_seeSalary = true;
                                break;
                        }
                    }
                    break;
                case 11:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 26:
                                state.jurisdiction.project_seeManage = true;
                                break;
                            case 27:
                                state.jurisdiction.project_seeSalary = true;
                                break;
                        }
                    }
                    break;
                case 12:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 28:
                                state.jurisdiction.performance_seeStatistics = true;
                                break;
                        }
                    }
                    break;
                case 13:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 29:
                                state.jurisdiction.bill_seeClientMonth = true;
                                break;
                        }
                    }
                    break;
                case 14:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 30:
                                state.jurisdiction.bill_sendMessage = true;
                                break;
                            case 31:
                                state.jurisdiction.bill_see = true;
                                break;
                        }
                    }
                    break;
                case 15:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 32:
                                state.jurisdiction.report_operating = true;
                                break;
                        }
                    }
                    break;
                case 16:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 33:
                                state.jurisdiction.commission_see = true;
                                break;
                            case 34:
                                state.jurisdiction.commission_add = true;
                                break;
                            case 35:
                                state.jurisdiction.commission_modify = true;
                                break;
                            case 36:
                                state.jurisdiction.commission_del = true;
                                break;
                        }
                    }
                    break;
                case 17:
                    for(let j = 0; j < info.user_role_auth[i].action_auth.length;j++){
                        switch(info.user_role_auth[i].action_auth[j].action_id){
                            case 37:
                                state.jurisdiction.messageManage_see = true;
                                break;
                            case 38:
                                state.jurisdiction.messageManage_add = true;
                                break;
                            case 39:
                                state.jurisdiction.messageManage_modify = true;
                                break;
                            case 40:
                                state.jurisdiction.messageManage_del = true;
                                break;
                        }
                    }
                    break;
            }
        }
        state.userInfo = info;
        state.login = true;
        localStorage.setStore('user_token', info.token);
        localStorage.setStore('user_shop_id', info.user_shop_id);
    },
    // 记录picker快递公司信息
    [GET_PICKERFMSLIST](state, data) {
        state.pickerFmsList = data;
    },
    // 记录picker快递公司信息
    [GET_PICKERBILLTYPELIST](state, data) {
        state.pickerBillTypeList = data;
    },
    // 记录picker职位信息
    [GET_PICKERJOBLIST](state, data) {
        state.pickerJobList = data;
    },
    // 记录picker项目单职位抽成信息
    [GET_PICKERBILLWORKLIST](state, data) {
        state.pickerBillWorkList = data;
    },
    // 记录picker项目单项目状态信息
    [GET_PICKERBOSSCONTENTLIST](state, data) {
        state.pickerBossContentList = data;
    },
    // 记录picker项目单工作状态信息
    [GET_PICKERWORKCONTENTLIST](state, data) {
        state.pickerWorkContentList = data;
    },
    // 记录picker支付方式信息
    [GET_PICKERPAYMENTLIST](state, data) {
        state.pickerPaymentList = data;
    },
    // 记录picker门店列表信息
    [GET_PICKERSHOPLIST](state, data) {
        state.pickerShopList = data;
    },
    // 记录picker门店角色列表信息
    [GET_PICKERSHOPROLELIST](state, data) {
        state.pickerShopRoleList = data;
    },
    // 缓存选中客户
    [TEMP_MANAGE](state, data) {
        state.tempMember = data;
    },
    // 缓存选中收件单
    [TEMP_RECFMS](state, data) {
        state.tempRecFms = data;
    },
    // 缓存选中相关人员
    [TEMP_PEOPLE](state, data) {
        state.tempPeople = data;
    },
    // 缓存选中项目单
    [TEMP_PROJECT](state, data) {
        state.tempProject = data;
    },

}
