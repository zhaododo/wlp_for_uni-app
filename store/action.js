import {
    getShopFmsList,
    getBillTypeList,
    getDictTypeList,
    getBillWorkList,
    getPaymentList,
    getShopRole,
    getShopNameList
} from '../service/getData'
import {
    GET_PICKERFMSLIST,
    GET_PICKERBILLTYPELIST,
    GET_PICKERJOBLIST,
    GET_PICKERBILLWORKLIST,
    GET_PICKERWORKCONTENTLIST,
    GET_PICKERBOSSCONTENTLIST,
    GET_PICKERPAYMENTLIST,
    GET_PICKERSHOPLIST,
    GET_PICKERSHOPROLELIST
} from './mutation-types.js'

export default {
    // 获取picker快递公司信息
    async getPickerFmsList({
        commit,
        state
    }) {
        if(state.pickerFmsList.length > 0) return;
        let res = await getShopFmsList();
        commit(GET_PICKERFMSLIST, res.result.data)
    },
    // 获取picker拍摄类型信息
    async getPickerBillTypeList({
        commit,
        state
    }) {
        if(state.pickerBillTypeList.length > 0) return;
        let res = await getBillTypeList();
        commit(GET_PICKERBILLTYPELIST, res.result.data)
    },
    // 获取picker职位信息
    async getPickerJobList({
        commit,
        state
    }) {
        if(state.pickerJobList.length > 0) return;
        let res = await getDictTypeList('position');
        commit(GET_PICKERJOBLIST, res.result.data)
    },
    // 获取picker项目单职位抽成信息
    async getPickerBillWorkList({
        commit,
        state
    },id) {
        commit(GET_PICKERBILLWORKLIST, []);
        let res = await getBillWorkList(id);
        commit(GET_PICKERBILLWORKLIST, res.result.data);
    },
    // 获取picker项目单工作状态信息
    async getPickerWorkContentList({
        commit,
        state
    }) {
        commit(GET_PICKERWORKCONTENTLIST, []);
        let res = await getDictTypeList('work_content');
        commit(GET_PICKERWORKCONTENTLIST, res.result.data);
    },
    // 获取picker项目单项目状态信息
    async getPickerBossContentList({
        commit,
        state
    }) {
        if(state.pickerBossContentList.length > 0) return;
        let res = await getDictTypeList('boss_content');
        commit(GET_PICKERBOSSCONTENTLIST, res.result.data);
    },
    // 获取picker支付方式信息
    async getPickerPaymentList({
        commit,
        state
    }) {
        if(state.pickerPaymentList.length > 0) return;
        let res = await getPaymentList();
        commit(GET_PICKERPAYMENTLIST, res.result.data);
    },
    // 获取picker门店列表信息
    async getPickerShopList({
        commit,
        state
    }) {
        if(state.pickerShopList.length > 0) return;
        let res = await getShopNameList();
        commit(GET_PICKERSHOPLIST, res.result.data);
    },
    // 获取picker门店角色列表信息
    async getPickerShopRoleList({
        commit,
        state
    }) {
        if(state.pickerShopRoleList.length > 0) return;
        let res = await getShopRole();
        commit(GET_PICKERSHOPROLELIST, res.result.data);
    },
}